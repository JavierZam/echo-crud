package router

import (
	"crud_echo/pkg/controller"
	"crud_echo/pkg/repository"
	"crud_echo/pkg/usecase"
	"database/sql"

	"github.com/labstack/echo/v4"
)

func NewUserRouter(e *echo.Echo, g *echo.Group, db *sql.DB) {
	ur := repository.NewUserRepository(db)
	uu := usecase.NewUserUsecase(ur)

	uc := &controller.UserController{
		UserUsecase: uu,
	}

	userGroup := g.Group("/user") 

	userGroup.GET("", uc.GetUsers)
	userGroup.GET("/:id", uc.GetUser)
	userGroup.POST("", uc.CreateUser)
	userGroup.PUT("/:id", uc.UpdateUser)
	userGroup.DELETE("/:id", uc.DeleteUser)

	e.POST("/login", uc.Login)
}
