package controller

import (
	"crud_echo/pkg/domain"
	"crud_echo/pkg/dto"
	"crud_echo/shared/response"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

type StudentController struct {
	StudentUsecase domain.StudentUsecase
}

func (sc *StudentController) handleStudentNotFound(c echo.Context, err error) error {
	return response.SetResponse(c, http.StatusNotFound, "id student not found", nil)
}

func (sc *StudentController) GetStudents(c echo.Context) error {
	resp, err := sc.StudentUsecase.GetStudents()
	if err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", resp)
}

func (sc *StudentController) GetStudent(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	resp, err := sc.StudentUsecase.GetStudent(id)
	if err != nil {
		return sc.handleStudentNotFound(c, err)
	}
	return response.SetResponse(c, http.StatusOK, "success", resp)
}

func (sc *StudentController) CreateStudent(c echo.Context) error {
	var studentDTO dto.StudentDTO
	if err := c.Bind(&studentDTO); err != nil {
		return response.SetResponse(c, http.StatusBadRequest, "bad request", nil)
	}
	if err := studentDTO.Validation(); err != nil {
		return response.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}
	if err := sc.StudentUsecase.CreateStudent(studentDTO); err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", nil)
}

func (sc *StudentController) UpdateStudent(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	_, err := sc.StudentUsecase.GetStudent(id)
	if err != nil { 
		return sc.handleStudentNotFound(c, err)
	}

	var studentdto dto.StudentDTO
	if err := c.Bind(&studentdto); err != nil {
		return response.SetResponse(c, http.StatusBadRequest, "bad request", nil)
	}
	if err := studentdto.Validation(); err != nil {
		return response.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}
	if err := sc.StudentUsecase.UpdateStudent(studentdto, id); err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", nil)
}

func (sc *StudentController) DeleteStudent(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	_, err := sc.StudentUsecase.GetStudent(id)
	if err != nil {
		return sc.handleStudentNotFound(c, err)
	}
	if err := sc.StudentUsecase.DeleteStudent(id); err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", nil)
}
