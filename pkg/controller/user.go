package controller

import (
	"net/http"
	"strconv"

	"crud_echo/pkg/domain"
	"crud_echo/pkg/dto"
	"crud_echo/shared/jwthelper"
	"crud_echo/shared/response"

	"github.com/labstack/echo/v4"
)

type UserController struct {
	UserUsecase domain.UserUsecase
}

func (uc *UserController) GetUsers(c echo.Context) error {
	resp, err := uc.UserUsecase.GetUsers()
	if err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", resp)
}

func (uc *UserController) GetUser(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	resp, err := uc.UserUsecase.GetUser(id)
	if err != nil {
		return response.SetResponse(c, http.StatusNotFound, "user with specified ID not found", nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", resp)
}

func (uc *UserController) CreateUser(c echo.Context) error {
	var userDTO dto.UserDTO
	if err := c.Bind(&userDTO); err != nil {
		return response.SetResponse(c, http.StatusBadRequest, "bad request", nil)
	}
	if err := userDTO.Validation(); err != nil {
		return response.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	if _, err := uc.UserUsecase.GetUserByEmail(userDTO.Email); err == nil {
		return response.SetResponse(c, http.StatusInternalServerError, "email already exists", nil)
	}

	if err := uc.UserUsecase.CreateUser(userDTO); err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", nil)
}

func (uc *UserController) UpdateUser(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	_, err := uc.UserUsecase.GetUser(id)
	if err != nil {
		return response.SetResponse(c, http.StatusNotFound, "user with specified ID not found", nil)
	}

	var userDTO dto.UserDTO
	if err := c.Bind(&userDTO); err != nil {
		return response.SetResponse(c, http.StatusBadRequest, "bad request", nil)
	}
	if err := userDTO.Validation(); err != nil {
		return response.SetResponse(c, http.StatusBadRequest, err.Error(), nil)
	}

	if err := uc.UserUsecase.UpdateUser(id, userDTO); err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return response.SetResponse(c, http.StatusOK, "success", nil)
}

func (uc *UserController) DeleteUser(c echo.Context) error {
	id, _ := strconv.Atoi(c.Param("id"))
	_, err := uc.UserUsecase.GetUser(id)
	if err != nil {
		return response.SetResponse(c, http.StatusNotFound, "user with specified ID not found", nil)
	}

	err = uc.UserUsecase.DeleteUser(id)
	if err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", nil)
}

func (uc *UserController) Login(c echo.Context) error {
	var userdto dto.UserDTO
	if err := c.Bind(&userdto); err != nil {
		return response.SetResponse(c, http.StatusBadRequest, "bad request", nil)
	}
	flag, err := uc.UserUsecase.Login(userdto.Email, userdto.Password)
	if err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, "email doesn't exist", nil)
	}
	if !flag {
		return response.SetResponse(c, http.StatusForbidden, "login failed", nil)
	}
	return response.SetResponse(c, http.StatusOK, jwthelper.CreateJWT(c, userdto.Username), nil)
}
