package usecase

import (
	"crud_echo/pkg/domain"
	"crud_echo/pkg/dto"
	"crud_echo/shared/util"

	"github.com/mitchellh/mapstructure"
)

type UserUsecase struct {
	UserRepository domain.UserRepository
}

func NewUserUsecase(userRepository domain.UserRepository) domain.UserUsecase {
	return &UserUsecase{
		UserRepository: userRepository,
	}
}

func (uu *UserUsecase) GetUsers() ([]domain.User, error) {
	return uu.UserRepository.GetUsers()
}

func (uu *UserUsecase) CreateUser(req dto.UserDTO) error {
	var user domain.User
	mapstructure.Decode(req, &user)

	hashedPassword, err := util.HashPassword(user.Password)
	if err != nil {
		return err
	}
	user.Password = hashedPassword

	return uu.UserRepository.CreateUser(user)
}

func (uu *UserUsecase) GetUser(id int) (domain.User, error) {
	return uu.UserRepository.GetUser(id)
}

func (uu *UserUsecase) UpdateUser(id int, req dto.UserDTO) error {
	var user domain.User
	mapstructure.Decode(req, &user)

	hashedPassword, err := util.HashPassword(user.Password)
	if err != nil {
		return err
	}
	user.Password = hashedPassword

	return uu.UserRepository.UpdateUser(id, user)
}

func (uu *UserUsecase) DeleteUser(id int) error {
	return uu.UserRepository.DeleteUser(id)
}

func (uu *UserUsecase) GetUserByEmail(email string) (domain.User, error) {
	return uu.UserRepository.GetUserByEmail(email)
}

func (uu *UserUsecase) Login(email string, password string) (bool, error) {
	user, err := uu.UserRepository.GetUserByEmail(email)
	if err != nil {
		return false, err
	}
	return util.CheckPasswordHash(password, user.Password), nil
}
