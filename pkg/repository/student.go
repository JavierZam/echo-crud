package repository

import (
	"crud_echo/pkg/domain"
	"database/sql"
)

type StudentRepository struct {
	db *sql.DB
}

func NewStudentRepository(db *sql.DB) domain.StudentRepository {
	return &StudentRepository{
		db: db,
	}
}

func (sr *StudentRepository) GetStudents() ([]domain.Student, error) {
	sql := `SELECT * FROM student ORDER BY id ASC`
	rows, err := sr.db.Query(sql)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var students []domain.Student
	for rows.Next() {
		var student domain.Student
		err2 := rows.Scan(&student.Id, &student.Fullname, &student.Address, &student.Birthdate, &student.Class, &student.Batch, &student.SchoolName)
		if err2 != nil {
			return nil, err2
		}
		students = append(students, student)
	}
	return students, nil
}

func (sr *StudentRepository) GetStudent(id int) (domain.Student, error) {
	var student domain.Student
	sql := `SELECT * FROM student WHERE id = $1`

	err := sr.db.QueryRow(sql, id).Scan(&student.Id, &student.Fullname, &student.Address, &student.Birthdate, &student.Class, &student.Batch, &student.SchoolName)
	if err != nil {
		return domain.Student{}, err
	}
	return student, nil
}

func (sr *StudentRepository) CreateStudent(req domain.Student) error {
	sql := `INSERT INTO student (fullname, address, birthdate, class, batch, school_name) values ($1, $2, $3, $4, $5, $6)`
	_, err := sr.db.Exec(sql, req.Fullname, req.Address, req.Birthdate, req.Class, req.Batch, req.SchoolName)
	return err
}

func (sr *StudentRepository) UpdateStudent(req domain.Student) error {
	sql := `UPDATE student SET fullname = $1, address = $2, birthdate = $3, class = $4, batch = $5, school_name = $6 WHERE id = $7`
	_, err := sr.db.Exec(sql, req.Fullname, req.Address, req.Birthdate, req.Class, req.Batch, req.SchoolName, req.Id)
	return err
}
 
func (sr *StudentRepository) DeleteStudent(id int) error {
	sql := `DELETE FROM student WHERE id = $1`
	_, err := sr.db.Exec(sql, id)
	return err
}
