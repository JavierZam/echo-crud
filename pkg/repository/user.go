package repository

import (
	"crud_echo/pkg/domain"
	"database/sql"
)

type UserRepository struct {
	db *sql.DB // db connection
}

func NewUserRepository(db *sql.DB) domain.UserRepository {
	return &UserRepository{
		db: db,
	}
}

func (sr UserRepository) GetUsers() ([]domain.User, error) {
	sql := `SELECT id, username, email, address FROM Users`
	rows, err := sr.db.Query(sql)
	var users []domain.User
	for rows.Next() {
		var user domain.User
		err2 := rows.Scan(&user.ID, &user.Username, &user.Email, &user.Address)
		if err2 != nil {
			return nil, err2
		}
		users = append(users, user)
	}
	return users, err
}

func (sr UserRepository) GetUser(id int) (domain.User, error) {
	var user domain.User
	sql := `SELECT id, username, email, address FROM Users WHERE id = $1`

	err := sr.db.QueryRow(sql, id).Scan(&user.ID, &user.Username, &user.Email, &user.Address)
	return user, err
}

func (sr UserRepository) CreateUser(req domain.User) error {
	sql := `INSERT INTO Users (username, email, address, password) values ($1, $2, $3, $4)`
	stmt, err := sr.db.Prepare(sql)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err2 := stmt.Exec(req.Username, req.Email, req.Address, req.Password)
	if err2 != nil {
		return err2
	}
	return nil
}

func (sr UserRepository) UpdateUser(id int, req domain.User) error {

	sql := `
	UPDATE Users
	SET username = $1,
    email = $2,
    address = $3,
    password = $4
	WHERE id = $5;`

	stmt, err := sr.db.Prepare(sql)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err2 := stmt.Exec(req.Username, req.Email, req.Address, req.Password, id)
	if err2 != nil {
		return err2
	}
	return nil
}

func (sr UserRepository) DeleteUser(id int) error {

	sql := `DELETE FROM Users WHERE id=$1;`

	stmt, err := sr.db.Prepare(sql)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err2 := stmt.Exec(id)
	if err2 != nil {
		return err2
	}
	return nil

}

func (sr UserRepository) GetUserByEmail(email string) (domain.User, error) {
	var user domain.User
	sql := `SELECT * FROM Users WHERE email = $1`

	err := sr.db.QueryRow(sql, email).Scan(&user.ID, &user.Username, &user.Email, &user.Address, &user.Password)
	return user, err
}
