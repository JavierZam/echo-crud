package domain

import "crud_echo/pkg/dto"

type User struct {
	ID       int    `json:"id"`
	Username string `json:"username"`
	Email    string `json:"email"`
	Address  string `json:"address"`
	Password string `json:"password"`
}

type UserDTO struct {
	Username string `json:"username" validate:"required"`
	Email    string `json:"email" validate:"required,email"`
	Address  string `json:"address" validate:"required"`
	Password string `json:"password" validate:"required"`
}

type UserRepository interface {
	GetUsers() ([]User, error)
	GetUser(id int) (User, error)
	CreateUser(req User) error
	UpdateUser(id int, req User) error
	DeleteUser(id int) error
	GetUserByEmail(email string) (User, error)
}

type UserUsecase interface {
	GetUsers() ([]User, error)
	GetUser(id int) (User, error)
	CreateUser(req dto.UserDTO) error
	UpdateUser(id int, req dto.UserDTO) error
	DeleteUser(id int) error
	GetUserByEmail(email string) (User, error)
	Login(email, password string) (bool, error)
}

type userUsecase struct {
	userRepository UserRepository
}

func NewUserUsecase(userRepository UserRepository) UserUsecase {
	return &userUsecase{
		userRepository: userRepository,
	}
}

func (uu *userUsecase) GetUsers() ([]User, error) {
	return uu.userRepository.GetUsers()
}

func (uu *userUsecase) GetUser(id int) (User, error) {
	return uu.userRepository.GetUser(id)
}

func (uu *userUsecase) CreateUser(req dto.UserDTO) error {
	user := User{
		Username: req.Username,
		Email:    req.Email,
		Address:  req.Address,
		Password: req.Password,
	}

	return uu.userRepository.CreateUser(user)
}

func (uu *userUsecase) UpdateUser(id int, req dto.UserDTO) error {
	user := User{
		Username: req.Username,
		Email:    req.Email,
		Address:  req.Address,
		Password: req.Password,
	}

	return uu.userRepository.UpdateUser(id, user)
}

func (uu *userUsecase) DeleteUser(id int) error {
	return uu.userRepository.DeleteUser(id)
}

func (uu *userUsecase) GetUserByEmail(email string) (User, error) {
	return uu.userRepository.GetUserByEmail(email)
}

func (uu *userUsecase) Login(email, password string) (bool, error) {
	user, err := uu.userRepository.GetUserByEmail(email)
	if err != nil {
		return false, err
	}

	return user.Password == password, nil
}
