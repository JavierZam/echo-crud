package cmd

import (
	"crud_echo/config"
	"crud_echo/pkg/router"
	"crud_echo/shared/db"

	"github.com/labstack/echo/v4"
)

func RunServer() {
	e := echo.New()
	g := e.Group("")

	conf := config.GetConfig()
	Apply(e, g, &conf)

	e.Logger.Error(e.Start(":5000"))
}

func Apply(e *echo.Echo, g *echo.Group, conf *config.Configuration) {
	db := db.NewInstanceDb()
	router.NewStudentRouter(e, g, db)
	router.NewUserRouter(e, g, db)
}
