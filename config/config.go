package config

import (
	"github.com/labstack/echo/v4/middleware"
	"github.com/tkanos/gonfig"
)

type Configuration struct {
	DBUsername    string `json:"db_username"`
	DBPassword    string `json:"db_password"`
	DBHost        string `json:"db_host"`
	DBPort        int    `json:"db_port"`
	DBName        string `json:"db_name"`
	JWTSecret     string `json:"jwt_secret"`
	JWTExpiryHour int    `json:"jwt_expiry_hour"`
}

var conf Configuration

func GetConfig() Configuration {
	err := gonfig.GetConf("config/config.json", &conf)
	if err != nil {
		panic(err)
	}
	return conf
}

func (c *Configuration) JWTConfig() middleware.JWTConfig {
	return middleware.JWTConfig{
		SigningKey: []byte(c.JWTSecret),
	}
}