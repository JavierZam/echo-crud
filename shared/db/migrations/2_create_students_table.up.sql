CREATE TABLE student (
    id SERIAL PRIMARY KEY,
    fullname VARCHAR(255),
    address VARCHAR(255),
    birthdate DATE,
    class VARCHAR(10),
    batch VARCHAR(10),
    school_name VARCHAR(255)
);
